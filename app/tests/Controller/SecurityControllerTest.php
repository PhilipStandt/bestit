<?php

declare(strict_types=1);

namespace App\Tests\Controller;


use App\Tests\AbstractControllerWebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class SecurityControllerTest extends AbstractControllerWebTestCase
{
    public function testLoginAndLogout(): void
    {
        // 400 Bad request when not supplying username and password
        $this->JSONRequest(Request::METHOD_POST, '/api/v1/login');
        $this->assertJSONResponse($this->client->getResponse(), Response::HTTP_BAD_REQUEST);

        // 401 Unauthorized when supplying wrong username and password
        $this->JSONRequest(Request::METHOD_POST, '/api/v1/login', ["username" => "wrong", "password" => "wong"]);
        $this->assertJSONResponse($this->client->getResponse(), Response::HTTP_UNAUTHORIZED);

        // Login with correct credentials
        $this->login();

        // Logout
        $this->logout();
    }
}