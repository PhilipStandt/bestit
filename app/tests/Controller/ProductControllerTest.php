<?php

declare(strict_types=1);

namespace App\Tests\Controller;


use App\Fixtures\ProductFixtures;
use App\Tests\AbstractControllerWebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ProductControllerTest extends AbstractControllerWebTestCase
{
    public function testGetProductsAction(): void
    {
        // Access without login, expect 401 Unauthorized
        $this->JSONRequest(Request::METHOD_GET, '/api/v1/product');
        $this->assertJSONResponse($this->client->getResponse(), Response::HTTP_UNAUTHORIZED);

        // Login
        $this->login();

        // Test if endpoint returns valid data
        $this->JSONRequest(Request::METHOD_GET, '/api/v1/product');
        $response = $this->assertJSONResponse($this->client->getResponse(), Response::HTTP_OK);

        $this->assertCount(1, $response);
        $this->assertEquals(ProductFixtures::ARTICLE_NUMBER, $response[0]["articleNumber"]);
        $this->assertEquals(ProductFixtures::NAME, $response[0]["name"]);
        $this->assertEquals(ProductFixtures::DESCRIPTION, $response[0]["description"]);
        $this->assertEquals(ProductFixtures::PRICE, $response[0]["price"]);

    }
}