<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User implements UserInterface {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank
     * @var string|null
     */
    private $login;

    /**
     * @ORM\Column(type="string")
     * @var string|null
     */
    private $password;

    /**
     * @ORM\Column(name="roles", type="simple_array")
     * @var string[]
     */
    private $roles = [];

    /**
     * @Assert\NotBlank(groups={"Registration"})
     */
    private $plainPassword;

    public function __construct()
    {
        $this->roles = [];
    }


    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles(array $roles): void {
        $this->roles = $roles;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password): void {
        $this->password = $password;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $password): void
    {
        $this->plainPassword = $password;
        $this->password = null;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->login;
    }

    public function setUsername(string $login): void
    {
        $this->login = $login;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }
}