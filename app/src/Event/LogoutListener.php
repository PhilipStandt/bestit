<?php

declare(strict_types=1);

namespace App\Event;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class LogoutListener
{
    public function onSymfonyComponentSecurityHttpEventLogoutEvent(LogoutEvent $event): void
    {
        $event->setResponse(new JsonResponse(['success' => true]));
    }
}