<?php

declare(strict_types=1);

namespace App\Controller;


use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ProductController
 * @Route("/api/v1/product")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 * @package App\Controller
 */
final class ProductController extends AbstractController
{

    /** @var EntityManagerInterface */
    private $em;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->serializer = $serializer;
    }

    /**
     * Returns a list of all products
     * @Route("", name="api_product_getall", methods={"GET"})
     * @return JsonResponse
     */
    public function getAllAction(): JsonResponse
    {
        $products = $this->em->getRepository(Product::class)->findBy([], ['name' => 'ASC']);
        $data = $this->serializer->serialize($products, JsonEncoder::FORMAT);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

}