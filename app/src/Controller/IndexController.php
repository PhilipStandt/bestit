<?php 

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class IndexController extends AbstractController
{
    /**
     * Use Vue for everything except /api and the symfony profiler
     * @Route("/{route}", requirements={"route"="^(?!api|_(profiler|wdt)).*"}, name="index")
     * @return Response
     */
    public function indexAction(): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        $data = null;
        if (!empty($user)) {
            $data = ["username" => $user->getUsername()];
        }
        return $this->render('base.html.twig', [
            'isAuthenticated' => json_encode(!empty($user)),
            'user' => json_encode($data)
        ]);
    }
}