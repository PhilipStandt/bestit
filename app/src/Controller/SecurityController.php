<?php


namespace App\Controller;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/v1")
 * Class SecurityController
 * @package App\Controller
 */
final class SecurityController extends AbstractController
{
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/login", methods={"POST"})
     * @return JsonResponse
     */
    public function loginAction(): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $data = ["username" => $user->getUsername(), "roles" => $user->getRoles()];

        $json = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/logout", methods={"POST"})
     */
    public function logoutAction(): void
    {
    }
}