<?php


namespace App\Fixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public const USERNAME = "testuser";
    public const PASSWORD = "password";

    public function load(ObjectManager $manager)
    {
        $userEntity = new User();
        $userEntity->setUsername(self::USERNAME);
        $userEntity->setPlainPassword(self::PASSWORD);
        $userEntity->setRoles(['ROLE_USER']);
        $manager->persist($userEntity);
        $manager->flush();
    }
}