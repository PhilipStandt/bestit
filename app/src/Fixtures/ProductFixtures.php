<?php


namespace App\Fixtures;


use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{

    public const ARTICLE_NUMBER = 1000;
    public const NAME = "testproduct";
    public const DESCRIPTION = "desc";
    public const PRICE = 9.99;

    public function load(ObjectManager $manager)
    {
        $product = new Product();
        $product->setName(self::NAME);
        $product->setArticleNumber(self::ARTICLE_NUMBER);
        $product->setDescription(self::DESCRIPTION);
        $product->setPrice(self::PRICE);

        $manager->persist($product);
        $manager->flush();
    }
}