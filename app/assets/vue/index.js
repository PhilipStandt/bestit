import Vue from "vue";
import App from "./views/App";
import router from "./router";
import store from "./store";
import './style.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

new Vue({
  components: { App },
  template: "<App/>",
  router,
  store
}).$mount("#app");