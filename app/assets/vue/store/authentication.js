import AuthApi from '../api/authentication';

const AUTHENTICATION_LOADING = "AUTHENTICATION_LOADING";
const AUTHENTICATION_SUCCESS = "AUTHENTICATION_SUCCESS";
const AUTHENTICATION_ERROR = "AUTHENTICATION_ERROR";
const AUTHENTICATION_LOADING_LOGOUT = "AUTHENTICATION_LOADING_LOGOUT";
const AUTHENTICATION_RELOAD = "AUTHENTICATION_RELOAD";

export default {
    namespaced: true,
    state: {
        username: null,
        authenticated: false,
        loading: false,
        error: null
    },
    getters: {
        hasError(state) {
            return state.error != null;
        },

        error(state) {
            return state.error;
        },

        isLoading(state) {
            return state.loading
        },

        isAuthenticated(state) {
            return state.authenticated;
        },

        username(state) {
            return state.username;
        }
    },
    actions: {
        async login({ commit }, data) {
            commit(AUTHENTICATION_LOADING);
            try {
                let response = await AuthApi.login(data.username, data.password);
                commit(AUTHENTICATION_SUCCESS, response.data);
                return response.data;
            } catch (error) {
                commit(AUTHENTICATION_ERROR, error);
                return null;
            }
        },

        async logout({ commit }) {
            commit(AUTHENTICATION_LOADING_LOGOUT);
            try {
                let response = await AuthApi.logout();
                commit(AUTHENTICATION_SUCCESS, null);
                return response.data;
            } catch (error) {
                commit(AUTHENTICATION_ERROR, error);
                return null;
            }
        },

        onRefresh({commit}, data) {
            commit(AUTHENTICATION_RELOAD, data)
        }
    },
    mutations: {
        [AUTHENTICATION_LOADING](state) {
            state.loading = true;
            state.authenticated = false;
            state.username = null;
            state.error = null;
        },

        [AUTHENTICATION_SUCCESS](state, data) {
            if (data == null) {
                state.loading = false;
                state.authenticated = false;
                state.username = null;
                state.error = null;
            } else {
                state.loading = false;
                state.authenticated = true;
                state.username = data.username;
                state.error = null;
            }
        },

        [AUTHENTICATION_ERROR](state, error) {
            state.loading = false;
            state.authenticated = false;
            state.username = null;
            state.error = error;
        },

        [AUTHENTICATION_LOADING_LOGOUT](state) {
            state.loading = true;
            state.authenticated = true;
        },

        [AUTHENTICATION_RELOAD](state, data) {
            state.isLoading = false;
            state.authenticated = data.isAuthenticated;
            state.username = data.user.username;
            state.error = null;
        }
    }
}