import Vue from "vue";
import Vuex from "vuex";
import ProductStore from "./product";
import AuthStore from "./authentication";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth: AuthStore,
        product: ProductStore
    }
});