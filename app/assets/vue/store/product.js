import ProductApi from "../api/product"

const LOAD_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
const LOAD_PRODUCTS_ERROR = 'GET_PRODUCTS_ERROR';

export default {
    namespaced: true,
    state: {
        products: [],
        error: null,
        loading: true
    },
    getters: {
        isLoading(state) {
            return state.loading;
        },

        hasError(state) {
            return state.error != null;
        },

        error(state) {
            return state.error;
        },

        hasProducts(state) {
            return state.products.length > 0;
        },

        products(state) {
            return state.products;
        }
    },
    actions: {
        async getAll({ commit }) {
            try {
                let response = await ProductApi.getAllProducts();
                commit(LOAD_PRODUCTS_SUCCESS, response.data);
                return response.data;
            } catch (error) {
                commit(LOAD_PRODUCTS_ERROR, error);
                return null;
            }
        }
    },
    mutations: {
        [LOAD_PRODUCTS_SUCCESS](state, products) {
            state.products = products;
            state.error = null;
            state.loading = false;
        },

        [LOAD_PRODUCTS_ERROR](state, error) {
            state.products = [];
            state.error = error;
            state.loading = false;
        }
    }
}