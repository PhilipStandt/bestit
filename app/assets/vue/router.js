import Vue from "vue";
import VueRouter from "vue-router";
import Products from "./views/Products";
import Login from "./views/Login";
import store from "./store";

Vue.use(VueRouter)

let router = new VueRouter({
    mode: "history",
    routes: [
        {path: "/login", component: Login},
        {path: "/products", component: Products, meta: {requiresAuth: true}},
        {path: "*", redirect: "/products"}
    ]
});

// Redirect to login page if next page requires authentication and the user is not authenticated
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters["auth/isAuthenticated"]) {
            next();
        } else {
            next({
                path: "/login",
            });
        }
    } else {
        next();
    }
})

export default router;